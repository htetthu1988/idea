<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

class MY_Controller extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        $this->assets = $this->config->item('assets');
    }

    
}
if ( ! class_exists('Admin_Controller'))
{
    require_once APPPATH.'core/Admin_Controller.php';
}
