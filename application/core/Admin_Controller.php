<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

class Admin_Controller extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin_model');
        $this->load->library('breadcrumb_lib');
        $this->layout->setLayout('admin');
        $this->assets = $this->config->item('assets').'admin/';
        $this->site_url = base_url('cms').'/';
        $this->isLogin();
        $this->login_name = $this->getAdminInfo('name');
        $this->permission = $this->getAdminInfo('permission');
        $this->admin_id = $this->getAdminInfo('id');
        $this->status = $this->config->item('status');
    }
    private function isLogin()
    {
        if(empty($this->session->userdata('admin_id'))){
            redirect(base_url('cms/login'));
        }
    }
    private function getAdminInfo($val)
    {
        if (!empty($this->session->userdata('admin_id'))) {
            $request['id'] = $this->session->userdata('admin_id');
            $admin = $this->admin_model->getAdmin($request);
            return v($admin,$val);
        }
    }

    public function clear()
    {
        if (!empty($this->session->userdata('conditions'))) {
            $this->session->unset_userdata('conditions');
        }
        redirect(base_url('cms/'.$this->router->fetch_class()));
    }
}
