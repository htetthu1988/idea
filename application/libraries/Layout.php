<?php
class Layout {
    var $template_data = array();
    var $layout="default";
    function set($content_area, $value)
    {
        $this->template_data[$content_area] = $value;
    }

    function load($view = '' , $view_data = array())
    {
        $this->CI =& get_instance();
        $this->set("contents" , $this->CI->load->view($view, $view_data, TRUE));
        $this->CI->load->view('layouts/'.$this->layout, $this->template_data);
    }
    function setLayout($layout_name)
    {
        $this->layout = $layout_name;
    }
}