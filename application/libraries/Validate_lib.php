<?php
class Validate_lib{
    public function __construct()
    {
        $this->CI = & get_instance ();
        $this->CI->load->library('form_validation');
        $this->CI->load->library('my_form_validation');
    }

    public function news()
    {
        $this->CI->form_validation->set_rules('new[publish_from]', 'PUBLISH FROM', 'required');
        $this->CI->form_validation->set_rules('new[publish_to]', 'PUBLISH TO', 'required');
        $this->CI->form_validation->set_rules('new[content]', 'NEWS', 'required');
        $this->CI->form_validation->set_rules('new[status]', 'STATUS', 'required');
    }
}
?>
