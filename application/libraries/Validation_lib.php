<?php
class Validation_lib{
    public function __construct()
    {
        $this->CI = & get_instance ();
        $this->CI->load->library('form_validation');
        $this->CI->load->library('my_form_validation');
    }

    public function rental()
    {
        $this->CI->form_validation->set_rules('insurance', 'レンタル安心補償 は必須です。', 'required');
        $this->CI->form_validation->set_rules('agree', ' ', 'required');
        $this->CI->form_validation->set_rules('rentals[0]', 'レンタル品', 'callback_rentals_check');
        $this->CI->form_validation->set_message('required', '{field} チェックしてください。');
    }
}
?>
