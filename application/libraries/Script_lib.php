<?php
class Script_lib{
    private $scripts;
    private $css_files;
    private $ele_js;
    public function __construct()
    {
        $this->CI = & get_instance ();
        $this->scripts = [];
        $this->css_files=[];
        $this->ele_js=[];
    }

    public function set_script($script){
        if(!empty($script)){
            $this->scripts[] = $script;
        }
    }
    public function load_script()
    {
        if(!empty($this->scripts)){
            $data['scripts'] = $this->scripts;
            $this->CI->load->view('elements/load_script', $data);
        }
    }
    public function set_css($css){
        if(!empty($css)){
            $this->css_files[] = $css;
        }
    }
    public function load_css()
    {
        if(!empty($this->css_files)){
            $data['css'] = $this->css_files;
            $this->CI->load->view('elements/load_css', $data);
        }
    }

    public function set_ele_js($file){
        if(!empty($file)){
            $this->ele_js[] = $file;
        }
    }
    public function load_ele_js()
    {
        if(!empty($this->ele_js)){
            foreach($this->ele_js as $ele)
            {
                $this->CI->load->view($ele);
            }
        }
    }
}
?>
