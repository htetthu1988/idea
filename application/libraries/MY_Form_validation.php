<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

class MY_Form_validation extends CI_Form_validation {
    function __construct()
    {
        parent::__construct();
		$this->set_error_delimiters('<div class="errorText"><label class="text-danger">※', '</label></div>');
    }


}
