<?php
class Curl_lib{
    public function __construct()
    {
        $this->CI = & get_instance ();
        //$this->token = $this->CI->config->item('token');
        $this->weather_api = $this->CI->config->item('weather_api');
    }

    // public function post_request($data)
    // {
    //     $ch = curl_init($this->token['api_url']);
    //     curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    //     curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    //     /* set return type json */
    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //     /* execute request */
    //     $result = curl_exec($ch);
    //     /* close cURL resource */
    //     curl_close($ch);
    //     return json_decode($result, true);
    // }

    public function get_weather_request()
    {
        $curl = curl_init($this->weather_api);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, true);   // ヘッダーも出力する
        /* execute request */
        $response = curl_exec($curl);
        // ステータスコード取得
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        // header & body 取得
        $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE); // ヘッダサイズ取得
        $body = substr($response, $header_size); // bodyだけ切り出し
        if($code==200){
            return json_decode($body, true);
        }
        return [];
    }
}
?>