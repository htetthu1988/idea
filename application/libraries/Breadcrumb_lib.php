<?php
class Breadcrumb_lib{
    private $bc;

    public function __construct()
    {
        $this->CI = & get_instance ();
        $this->bc = [];
        $this->title = '';
    }

    public function set_bc($bc_arr)
    {
        $this->bc['bc_items'][] = $bc_arr;
    }

    public function show_bc()
    {
        if(!empty($this->bc)){
            $this->CI->load->view('elements/admin/breadcrumb', $this->bc);
        }
    }

    public function set_title($title)
    {
        if(!empty($title)){
            $this->title = $title;
        }
    }
    public function load_title()
    {
        if(!empty($this->title)){
            return $this->title;
        }
    }
}
?>
