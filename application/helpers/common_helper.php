
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if (!function_exists('v')) {
    function v($array, $key, $default = '')
    {
        if (isset($array[$key])) {
            return $array[$key];
        }
        return $default;
    }
}
if (!function_exists('arrayCollection')) {
    function arrayCollection($arrays, $key)
    {
        $results=[];
        if (!empty($arrays)) {
            foreach ($arrays as $array) {
                $results[] = $array[$key];
            }
        }
        return $results;
    }
}
/**
 * 配列から改行区切り文字列を取得
 * @param array $array
 * @return string
 */
if (!function_exists('arrayToStringByEol')) {
    function arrayToStringByEol($array)
    {
        $str = "";
        if (!empty($array)) {
            foreach ($array as $val) {
                if($val !=''){
                    $str .= $val. PHP_EOL;
                }
            }
        }
        return $str;
    }
}
/**
 * 改行区切り文字列から配列を取得
 * @param string $str
 * @return array
 */
if (!function_exists('stringToArrayByEol')) {
    function stringToArrayByEol($str="")
    {
        if (empty($str)) {
            return [];
        }
        $array = explode(PHP_EOL, $str); // とりあえず行に分割
        $array = array_map('trim', $array); // 各行にtrim()をかける
        $array = array_filter($array, 'strlen'); // 文字数が0の行を取り除く
        $array = array_values($array); // これはキーを連番に振りなおしてるだけ
        return $array;
    }
}
if (!function_exists('getEntities')) {
    function getEntities($mFileds){
        $entities = [];
        if(!empty($mFileds)){
            foreach($mFileds as $filed){
                $entities[$filed] = '';
            }
        }
        return $entities;
    }
}

if (!function_exists('h')) {
    function h($text, $double = true, $charset = null)
    {
        if (is_string($text)) {
            //optimize for strings
        } elseif (is_array($text)) {
            $texts = [];
            foreach ($text as $k => $t) {
                $texts[$k] = h($t, $double, $charset);
            }

            return $texts;
        } elseif (is_object($text)) {
            if (method_exists($text, '__toString')) {
                $text = (string)$text;
            } else {
                $text = '(object)' . get_class($text);
            }
        } elseif (is_bool($text) || is_null($text) || is_int($text)) {
            return $text;
        }

        static $defaultCharset = false;
        if ($defaultCharset === false) {
            $defaultCharset = mb_internal_encoding();
            if ($defaultCharset === null) {
                $defaultCharset = 'UTF-8';
            }
        }
        if (is_string($double)) {
            $charset = $double;
        }

        return htmlspecialchars($text, ENT_QUOTES | ENT_SUBSTITUTE, $charset ?: $defaultCharset, $double);
    }
}

if (!function_exists('getStatusText')) {
    function getStatusText($new){

        if(empty($new)){
            return '';
        }
        if( $new['publish_from'] <= date("Y-m-d H:i:s") &&  date("Y-m-d H:i:s") <= $new['publish_to'] && ($new['status']==1)){
            return '<span class="label label-mint">Public</span>';
        }else{
            return '<span class="label label-default">Private</span>';
        }
    }
}

