<?php $this->script_lib->set_ele_js('elements/front/js/map_js.php');?>
<?php $this->script_lib->set_css($this->assets.'front/css/map.css')?>
<div class="page-titles title-grey pt30 pb20">
    <div class="container">
        <div class="row">
            <div class=" col-md-6">
                <h4><span>MAP</span></h4>
            </div>
        </div>
    </div>
</div>
<div class="container mb70 pt90 layout-container">
    <input
        id="pac-input"
        class="controls"
        type="text"
        placeholder="Search Box"
    />
    <div id="map_container" style="width: 100%;height: 350px;position: relative;overflow: hidden;"></div>
</div>