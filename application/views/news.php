<div class="page-titles title-grey pt30 pb20">
    <div class="container">
        <div class="row">
            <div class=" col-md-6">
                <h4><span>CMS NEWS</span></h4>
            </div>
        </div>
    </div>
</div>
<div class="container mb70 pt90 layout-container">
    <?php /*
    <div class="news-div">
        <ul>
            <?php if(!empty($news)):?>
                <?php foreach($news as $new):?>

                    <li>
                        <div class="new-date mb05">
                            <?= date('Y-m-d H:i:s', strtotime($new['publish_from']))?>
                        </div>
                        <span class="news-content">
                            <?= nl2br(h($new['content']));?>
                        </span>
                    </li>
                <?php endforeach;?>
            <?php endif;?>
           
        </ul>
    </div> 
    */ ?>
    <?php if(!empty($news)):?>
        <div class="news-container">
            <?php foreach($news as $new):?>
                <div class="row">
                    <div class="col-3 news-text py-2">
                        <?= date('Y-m-d H:i:s', strtotime($new['publish_from']))?>
                    </div>
                    <div class="col-9 py-2"><?= nl2br(h($new['content']));?></div>
                </div>
            <?php endforeach;?>
        </div>
    <?php endif;?>
</div>