<!DOCTYPE html>
<html lang="ja">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>idea lump test</title>
    <!-- Plugins CSS -->
    <link href="<?= base_url()?>assets/front/css/plugins/plugins.css" rel="stylesheet">
    <!-- REVOLUTION STYLE SHEETS -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/front/revolution/css/settings.css">
    <!-- REVOLUTION LAYERS STYLES -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/front/revolution/css/layers.css">
    <!-- REVOLUTION NAVIGATION STYLES -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/front/revolution/css/navigation.css">
    <!-- load css for cubeportfolio -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/front/cubeportfolio/css/cubeportfolio.min.css">
    <link href="<?= base_url()?>assets/front/css/style.css" rel="stylesheet">
    <?php $this->script_lib->load_css();?>
	<link href="<?= base_url()?>assets/front/css/idea.css?<?=time()?>" rel="stylesheet">
</head>

<body>
    <div id="preloader">
        <div id="preloader-inner"></div>
    </div>
    <div class="site-overlay"></div>

    <nav class="navbar navbar-expand-lg navbar-light bg-faded nav-sticky-top">
        <!--/search form-->
        <div class="container">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="<?= base_url()?>">
                <p class="logo">
                    <h1>Nexidea Test Page</h1>
                </p>
            </a>
            <?php require(APPPATH."views/elements/front/navigation.php");?>
            <!--right nav icons-->
        </div>
    </nav>
    <?= !empty($contents) ?$contents:'';?>
    <!-- <div class="container mb70 pt90 layout-container" id='more'>
        
    </div> -->
    <?php require(APPPATH."views/elements/front/footer.php");?>
  


    <!--back to top-->
    <a href="#" class="back-to-top hidden-xs-down" id="back-to-top"><i class="ti-angle-up"></i></a>
    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="<?= base_url()?>assets/front/js/plugins/plugins.js"></script>
    <script src="<?= base_url()?>assets/front/js/assan.custom.js"></script>
    <script src="<?= base_url()?>assets/js/loadingoverlay.min.js"></script>
    <script src="<?= base_url()?>assets/js/loadingoverlay_progress.min.js"></script>
    <script src="<?= base_url()?>assets/js/load-overlay.js"></script>
	<script src="<?= base_url()?>assets/js/popper.min.js"></script>
	<script src="<?= base_url()?>assets/js/jquery-ui.min.js"></script>
    <script 
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD1UG7miji9nFecz1zPncfmSK1KIQbkQxk&callback=initMap&libraries=places&v=weekly" 
        async
    ></script>
    <?php $this->script_lib->load_script();?>
    <?php $this->script_lib->load_ele_js();?>
</body>

</html>
