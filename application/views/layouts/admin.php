<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?= SITE_TITLE?>-(ADMIN)</title>
		<link rel="shortcut icon" href="<?= base_url()?>assets/img/favicon.ico" type="image/vnd.microsoft.icon">
        <!--STYLESHEET-->
        <!--=================================================-->
        <!--Bootstrap Stylesheet [ REQUIRED ]-->
        <link href="<?= $this->assets?>css/bootstrap.min.css" rel="stylesheet">
        <!--Jasmine Stylesheet [ REQUIRED ]-->
        <link href="<?= $this->assets?>css/style.css" rel="stylesheet">
        <link href="<?= $this->assets?>css/main.css?<?= date('Y-m-d H:i:s')?>" rel="stylesheet">
        <!--Font Awesome [ OPTIONAL ]-->
        <link href="<?= base_url('assets')?>/fontawesome/css/all.min.css" rel="stylesheet">
        <link href="<?= $this->assets?>plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?= $this->assets?>plugins/animsition/css/animsition.min.css" rel="stylesheet">
        <link href="<?= $this->assets?>plugins/animate-css/animate.min.css" rel="stylesheet">
        <?php $this->script_lib->load_css();?>
        <!--SCRIPT-->
        <!--=================================================-->
    </head>
    <!--TIPS-->
    <!--You may remove all ID or Class names which contain "demo-", they are only used for demonstration. -->
    <!-- <body 
        class="animsition-overlay" 
        data-animsition-in-class="fade-in"
        data-animsition-in-duration="1000"
        data-animsition-out-class="fade-out"
        data-animsition-out-duration="800"
    > -->
    <body>
        <div id="container" class="effect navbar-fixed mainnav-fixed mainnav-lg">
            <?php require(APPPATH."views/elements/admin/header.php");?>
            <!--===================================================-->
            <!--END NAVBAR-->
            <div class="boxed">
                <!--CONTENT CONTAINER-->
                <!--===================================================-->
                <div id="content-container">
                    <?php require(APPPATH."views/elements/admin/breadcrumb_container.php");?>
                    <!--Page content-->
                    <!--===================================================-->
                    <div id="page-content">
                        <?= !empty($contents) ?$contents:'';?>
                    </div>
                    <!--===================================================-->
                    <!--End page content-->
                </div>
                <!--===================================================-->
                <!--END CONTENT CONTAINER-->
                <?php require(APPPATH."views/elements/admin/side_menu.php");?>
            </div>
            <?php require(APPPATH."views/elements/admin/footer.php");?>
            <!-- SCROLL TOP BUTTON -->
            <!--===================================================-->
            <button id="scroll-top" class="btn"><i class="fa fa-chevron-up"></i></button>
            <!--===================================================-->
        </div>
        <!--===================================================-->
        <!-- END OF CONTAINER -->
        <!--JAVASCRIPT-->
        <!--=================================================-->
        <!--jQuery [ REQUIRED ]-->
        <script src="<?= $this->assets?>js/jquery-2.1.1.min.js"></script>
        <script src="<?= $this->assets?>js/jquery-ui.min.js"></script>
        <!--BootstrapJS [ RECOMMENDED ]-->
        <script src="<?= $this->assets?>js/bootstrap.min.js"></script>
        <!--Fast Click [ OPTIONAL ]-->
        <script src="<?= $this->assets?>plugins/fast-click/fastclick.min.js"></script>
        <!--Jquery Nano Scroller js [ REQUIRED ]-->
        <script src="<?= $this->assets?>plugins/nanoscrollerjs/jquery.nanoscroller.min.js"></script>
        <!--Metismenu js [ REQUIRED ]-->
        <script src="<?= $this->assets?>plugins/metismenu/metismenu.min.js"></script>
        <!--Jasmine Admin [ RECOMMENDED ]-->
        <script src="<?= $this->assets?>js/scripts.js"></script>
        <!--Fullscreen jQuery [ OPTIONAL ]-->
        <script src="<?= $this->assets?>plugins/screenfull/screenfull.js"></script>
        <script src="<?= $this->assets?>plugins/animsition/js/animsition.min.js"></script>
        <script src="<?= $this->assets?>js/demo/ui-animation.js"></script>
        <script src="<?= $this->assets?>js/bootbox.all.min.js"></script>
        <script src="<?= $this->assets?>js/modal-error.js"></script>
        <script src="<?= base_url()?>assets/js/loadingoverlay.min.js"></script>
        <script src="<?= base_url()?>assets/js/loadingoverlay_progress.min.js"></script>
        <script src="<?= base_url()?>assets/js/load-overlay.js"></script>
        <?php $this->script_lib->load_script();?>
        <?php $this->script_lib->load_ele_js();?>
        <?php require(APPPATH."views/elements/admin/js/animsition_custom_js.php");?>
        <script>
            $(document).ready(function(){
                $('[data-toggle="tooltips"]').tooltip();
                $('.main-menu').click(function(){
                    localStorage.setItem('sub_menu', 'false');
                    localStorage.setItem('mail_sub_menu', 'false');
                });
                $('.sub-menu').click(function(){
                    localStorage.setItem('sub_menu', 'true');
                    localStorage.setItem('mail_sub_menu', 'false');
                });
                $('.mail-sub-menu').click(function(){
                    localStorage.setItem('mail_sub_menu', 'true');
                    localStorage.setItem('sub_menu', 'false')
                });
                if(localStorage.getItem('sub_menu')=='true'){
                    $(".sub-menu-ul").addClass("in");
                }
                if(localStorage.getItem('mail_sub_menu')=='true'){
                    $(".mail-sub-menu-ul").addClass("in");
                }
            });
        </script>
    </body>
</html>
