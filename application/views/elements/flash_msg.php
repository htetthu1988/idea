<?php if(!empty($this->session->tempdata('error_flash'))):?>
    <div class="alert alert-danger" role="alert">
        <strong><i class="fa fa-warning"></i></strong> <?= $this->session->tempdata('error_flash');?>
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button" style="top:10px;">×</button>
    </div>
<?php endif;?>
<?php if(!empty($this->session->tempdata('success_flash'))):?>
    <div class="alert alert-success" role="alert">
        <strong><i class="fa fa-thumbs-o-up"></i></strong> <?= $this->session->tempdata('success_flash');?>
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button" style="top:10px;">×</button>
    </div>
<?php endif;?>