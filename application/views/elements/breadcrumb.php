<?php $this->script_lib->set_ele_js('element/js/form_submit_js.php');?>
<?php if(!empty($bc_items)):?>
    <?php
        $tmp = array_keys($bc_items);
        $last_key = end($tmp);
    ?>
    <div class="font-up-bold breadcrumb-container pc">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <?php foreach($bc_items as $indx=>$bc):?>
                    <?php if($last_key != $indx):?>
                        <li class="breadcrumb-item">
                            <a
                                class="submit-btn"
                                href="#"
                                data-url="<?= sprintf($bc['url'], base_url()) ?>"
                            >
                                <small class="mr-1 mb-0 ">
                                    <strong><?= $bc['text']?></strong>
                                </small>
                            </a>
                        </li>
                    <?php else:?>
                        <li class="breadcrumb-item active">
                            <small class="mr-1 mb-0 mt-1">
                                <strong><?= $bc['text']?></strong>
                            </small>
                        </li>
                    <?php endif;?>
                <?php endforeach;?>
            </ol>
        </nav>
    </div>
<?php endif;?>
