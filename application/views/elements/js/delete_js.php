<script>
    $(document).ready(function(){
        $(document).on('click', '.btn-delete', function() {
            var url = $(this).data('url');
            var msg = $(this).data('msg');
            bootbox.confirm({
                message: msg+"を削除します。よろしいですか？",
                buttons: {
                    confirm: {
                        label: '削除',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'いいえ',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if(result){
                        loading();
                        location.href=url;
                    }
                }
            });
        });
    });
</script>