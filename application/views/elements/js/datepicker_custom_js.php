

<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css">
<script src="<?= $this->assets?>js/jquery.ui.datepicker-en-GB.js"></script>
<script src="<?= $this->assets?>js/jquery.ui.datepicker-ja.min.js"></script>
<script src="<?= $this->assets?>js/jquery.ui.datepicker-zh-CN.js"></script>
<script>
    $(function() {
        $.datepicker.setDefaults($.datepicker.regional["en-GB"]);
        $('.datepicker').datepicker({
            dateFormat: 'yy/mm/dd',
            numberOfMonths: 1,
            // minDate: 0,
            // maxDate: '+6m'
            //showButtonPanel: true,
        })
    });
</script>
