<link rel="stylesheet" href="<?= $this->assets?>css/bootstrap-datetimepicker.css">
<script src="<?= $this->assets?>js/moment-with-locales.js"></script>
<script src="<?= $this->assets?>js/bootstrap-datetimepicker.js"></script>
<script>
    $(function() {
        $('.datetimepicker').datetimepicker({
            locale: 'en',
            format : 'YYYY/MM/DD HH:mm',
            useCurrent: false,
        });
        $('.date-picker').datetimepicker({
            locale: 'en',
            format : 'YYYY/MM/DD',
            useCurrent: false,
        });
    });
</script>