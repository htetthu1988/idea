<div class="form-group">
    <label class="control-label col-md-4">ID</label>
    <div class="col-md-8" style="padding-top: 7px;">
        <?= !empty($new['id'])?h($new['id']):'auto increment'?>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-4">PUBLISH FROM<span class="required">【必須】</span></label>
    <div class="col-md-8">
        <div class="form-group mar-btm-no">
            <div class="col-md-4">
                <input 
                    type="text" 
                    class="form-control datetimepicker"
                    name="new[publish_from]"
                    value="<?= h($new['publish_from'])?>"
                >
            </div>
        </div>
        <?= form_error('new[publish_from]'); ?>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-4">PUBLISH TO<span class="required">【必須】</span></label>
    <div class="col-md-8">
        <div class="form-group mar-btm-no">
            <div class="col-md-4">
                <input 
                    type="text" 
                    class="form-control datetimepicker"
                    name="new[publish_to]"
                    value="<?= h($new['publish_to'])?>"
                >
            </div>
        </div>
        <?= form_error('new[publish_to]'); ?>
    </div>
</div>

<div class="form-group">
    <label class="control-label col-md-4">NEWS<span class="required">【必須】</span></label>
    <div class="col-md-8">
        <textarea name="new[content]" class="form-control" rows="8"><?= h($new['content'])?></textarea>
        <?= form_error('new[content]'); ?>
    </div>
</div>

<div class="form-group">
    <label class="control-label col-md-4">STATUS<span class="required">【必須】</span></label>
    <div class="col-md-8">
        <input type="hidden" name="new[status]" value=""/>
        <div class="radio">
            <?php if(!empty($this->status)):?>
                <?php foreach($this->status as $indx =>$s):?>
                    <label>
                        <input 
                            type="radio" 
                            name="new[status]" 
                            value="<?= $indx?>"
                            <?= $new['status']==$indx ? 'checked':'';?>
                        ><?= $s?>
                    </label>　
                <?php endforeach;?>
            <?php endif;?>
        </div>
        <?= form_error('new[status]'); ?>
    </div>
</div>