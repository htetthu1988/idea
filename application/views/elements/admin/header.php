<!--NAVBAR-->
<!--===================================================-->
<header id="navbar">
    <div id="navbar-container" class="boxed">
        <!--Navbar Dropdown-->
        <!--================================-->
        <div class="navbar-content clearfix">
            <ul class="nav navbar-top-links pull-left">
                <!--Navigation toogle button-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <li class="tgl-menu-btn">
                    <a class="mainnav-toggle" href="#"> <i class="fa fa-navicon fa-lg"></i> </a>
                </li>
                <li class="header-title">
                    <h3><?= SITE_TITLE;?></h3>
                </li>
            </ul>
            <ul class="nav navbar-top-links pull-right">
                <li id="dropdown-user" class="dropdown">
                    <a href="#" data-toggle="dropdown" class="dropdown-toggle text-right">
                        <span class="pull-right"> 
                            <img class="img-circle img-user media-object" src="<?= $this->assets?>img/admin_user.png" alt="Profile Picture"> 
                        </span>
                        <div class="username hidden-xs"><?= !empty($this->login_name)?$this->login_name:''?></div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right with-arrow">
                        <!-- User dropdown menu -->
                        <ul class="head-list">
                            <!-- <li>
                                <a href="<?= base_url()?>admin/home/changePassword"> <i class="fas fa-key"></i> パスワード変更 </a>
                            </li> -->
                            <li>
                                <a href="<?= base_url()?>cms/login/logout"> <i class="fa fa-sign-out fa-fw"></i> Logout </a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
        <!--================================-->
        <!--End Navbar Dropdown-->
    </div>
</header>
