<?php if(!empty($bc_items)):?>
    <?php
        $tmp = array_keys($bc_items);
        $last_key = end($tmp);
    ?>
     <span class="label">current page:</span>
    <ol class="breadcrumb">
        <?php foreach($bc_items as $indx=>$bc):?>
            <?php if($last_key != $indx):?>
                <li class="breadcrumb-item">
                    <a 
                        href="<?=$bc['url']?>"
                    >
                    <strong><?= $bc['text']?></strong>
                    </a>
                </li>
            <?php else:?>
                <li class="active">
                    <strong><?= $bc['text']?></strong>
                </li>
            <?php endif;?>
        <?php endforeach;?>
    </ol>
<?php endif;?>
