<link rel="stylesheet" type="text/css" href="<?=$this->assets?>plugins/datatables/media/css/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?=$this->assets?>plugins/datatables/extensions/Responsive/css/dataTables.responsive.css">
<!--DataTables [ OPTIONAL ]-->
<script src="<?=$this->assets?>plugins/datatables/media/js/jquery.dataTables.js"></script>
<script src="<?=$this->assets?>plugins/datatables/media/js/dataTables.bootstrap.js"></script>
<script src="<?=$this->assets?>plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=$this->assets?>js/demo/tables-datatables.js"></script>
<script type="text/javascript">
    jQuery(function($){
        $('#list').dataTable({
            "order": [[ 1, "desc" ]],
            "responsive": true,
            "pagingType": "full_numbers",
            //"language": getLanguageJson()
        });
    });
</script>
