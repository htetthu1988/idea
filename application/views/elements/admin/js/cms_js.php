<script type="text/javascript">
    jQuery(function($){
        $('.form-submit').click(function(){
            var url = $(this).data('url');
            bootbox.confirm({
                message:"保存します。よろしいですか？",
                buttons: {
                    confirm: {
                        label: '保存する',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: '保存しない',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if(result){
                        $('#form').attr('action', url);
                        $('#form').submit();
                    }
                }
            });
        });
    });
</script>
