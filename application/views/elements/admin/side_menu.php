<!--MAIN NAVIGATION-->
<!--===================================================-->
<nav id="mainnav-container">
    <!--Brand logo & name-->
    <!--================================-->
    <div class="navbar-header">
        <a href="<?= base_url()?>admin" class="navbar-brand">
            <!-- <i class="fas fa-umbrella-beach brand-icon"></i> -->
            <!-- <i class="fa fa-globe brand-icon"></i> -->
            <div class="brand-title">
                <span class="brand-text"></span>
            </div>
        </a>
    </div>
    <!--================================-->
    <!--End brand logo & name-->
    <div id="mainnav">
        <!--Menu-->
        <!--================================-->
        <div id="mainnav-menu-wrap">
            <div class="nano">
                <div class="nano-content">
                    <ul id="mainnav-menu" class="list-group">
                        <li> 
                            <a 
                                href="<?= $this->site_url;?>home/clear" 
                                class="main-menu animsition-link <?= strtolower($this->router->fetch_class())=='home'?'menu-active':''?>"
                            > 
                                <i class="fa fa-list"></i>
                                <span class="menu-title"> News list</span> 
                            </a> 
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--================================-->
        <!--End menu-->
    </div>
</nav>
<!--END MAIN NAVIGATION-->
