<div class="back-to-top"><i class="fas fa fa-angle-up"></i></div>
<style>
    .back-to-top {
        position: fixed;
        bottom: 20px;
        right: 10px;
        display: inline-block;
        padding: 0.5em 1em 0.5em 1em;
        margin: 0.5em;
        color: #fffbf4;
        background: #e1975787;
        border-radius: 50%;
    }
    .back-to-top:hover {
        cursor: pointer;
    }
    @media all and (-ms-high-contrast: none) {
        .back-to-top {
            position: fixed;
            bottom: 20px;
            right: 10px;
            display: inline-block;
            padding: 0.5em 1em 0.5em 1em;
            margin: 0.5em;
            color: #fffbf4;
            background: orange;
            border-radius: 50%;
        }
        .back-to-top:hover {
            cursor: pointer;
        }
    }
</style>

