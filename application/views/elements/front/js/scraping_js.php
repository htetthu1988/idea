<script type="text/javascript">

    $(document).ready(function() {
        //loadShopList();
    });
    var loadShopList = function ()
    {
        //var self = $(this);

        $.ajax({
            url: 'http://lumine.ne.jp/xml/cms_shop.xml',
            type: "GET",
            crossDomain: true,
            dataType: "xml",
            cache: false
        }).done(function(xml){

            var topics = $(xml).find('shopinfo');
            //var main = $(self);
            var domain = 'http://www.lumine.ne.jp';

            var count = 0;

            $(topics).each(function(i){
                var yakata_id = $(this).find('yakata_id').text();
                if(settings.yakataID == yakata_id){
                    var linkURL = 'news_details.php?id=' + $(this).find('id').text();
                    var srcURL = ($(this).find('shop_info').find('image').text() != '')? domain + '/pict/' + $(this).find('shop_info').find('image').text() : domain + '/common/img/noimage.gif';
                    var date = $(this).find('print_date').text();
                    var title = $(this).find('title').eq(0).text();
                    var new_icon = $(this).find('new_icon').text();

                    var shop_name = $(this).find('shop_name').text();
                    var shop_mansion_id = $(this).find('shop_mansion_id').text();
                    var mansion_name = '';
                    var shop_floor = $(this).find('shop_floor').text();

                    if (shop_mansion_id == '01' || shop_mansion_id == '04' || shop_mansion_id == '17') {
                        mansion_name = "ルミネ1/";
                    }
                    else if (shop_mansion_id == '02' || shop_mansion_id == '05' || shop_mansion_id == '18') {
                        mansion_name = "ルミネ2/";
                    }
                    var dateObj = new Date(date);
                    var mm = dateObj.getMonth() + 1;
                    var dateTxt = dateObj.getFullYear() + '.' + mm + '.' + dateObj.getDate();
                    var html = '<a href="'+linkURL+'" class="topics transition clearfix'+((count==0)?' fc':'')+'"><div class="topicsImg"><img src="'+srcURL+'" alt=""></div><div class="topicsInfo"><div class="topicsLabel"><span class="date">'+dateTxt+'</span>';
                    if(new_icon == '1'){
                        html += '<span class="newIcon"><img src="/common/img/new_icon.jpg" alt="NEW"></span>';
                    }

                    if (shop_name != ''){
                        html += '</div>【' + shop_name + '】' + mansion_name + shop_floor + ' ' + title + '</div></a>';
                    }
                    else{
                        html += '</div>'+title+'</div></a>';
                    }
                    var tmp = $(html);
                    //main.append(tmp);
                    count++;
                }

            });


         }).fail(function(jqXHR, textStatus, ex){
            alert(textStatus + "," + ex + "," + jqXHR.responseText);
        });

        return this;
    }
</script>