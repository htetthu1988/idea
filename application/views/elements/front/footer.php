<footer class="footer footer-dark pt30 pb30">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-10 ml-auto mr-auto text-center">
                <p class="mt-3">&copy; copyright . all right reserved</p>
            </div>
        </div>
    </div>
</footer>
