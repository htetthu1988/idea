<div id="navbarNavDropdown" class="navbar-collapse collapse">
    <ul class="navbar-nav ml-auto">
        <li class="nav-item <?= strtolower($this->router->fetch_class())=='weather'?'active':''?> ">
            <a class="nav-link " href="<?= base_url('weather')?>">Weather</a>
        </li>

        <li class="nav-item <?= strtolower($this->router->fetch_class())=='map'?'active':''?> ">
            <a class="nav-link " href="<?= base_url('map')?>">Map</a>
        </li>
    
        <li class="nav-item <?= strtolower($this->router->fetch_class())=='scraping'?'active':''?> ">
            <a class="nav-link " href="<?= base_url('scraping')?>">Scraping</a>
        </li>
    
        <li class="nav-item <?= strtolower($this->router->fetch_class())=='news'?'active':''?> ">
            <a class="nav-link " href="<?= base_url('news')?>">CMS news</a>
        </li>
    
    </ul>
</div>
