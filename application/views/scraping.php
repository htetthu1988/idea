<?php $this->script_lib->set_ele_js('elements/front/js/scraping_js.php'); ?>
<div class="page-titles title-grey pt30 pb20">
    <div class="container">
        <div class="row">
            <div class=" col-md-6">
                <h4><span>Scraping</span></h4>
            </div>
        </div>
    </div>
</div>
<div class="container mb70 pt90 layout-container scraping">
    <?php if(!empty($lists)):?>
        <?php foreach($lists as $list):?>
            <div class="row">
                <div class="col-2">
                    <img src="<?= h($list['image'])?>" style="width:100px;"/>
                </div>
                <div class="col-10">
                    <div class="row">
                        <div class="col-12">
                            <div class="pull-left" style="font-weight:bold">
                                <?= h($list['title'])?>
                                <?php if(!empty($list['new_icon'])):?>
                                    <img src="<?= h($list['new_icon'])?>" />
                                <?php endif;?>
                            </div>
                            <div class="pull-right text-info">
                                <?= !empty($list['print_date'])?date('Y年m月d日', strtotime($list['print_date'])):''?>
                            </div>
                        </div>
                    </div>
                    <?php if(!empty($list['content']['news_txt'])):?>
                        <div class="row my-3">
                            <div class="col-12">
                                <?= nl2br($list['content']['news_txt'])?>
                            </div>
                        </div>
                    <?php endif;?>
                    <?php if(!empty($list['content']['shop_info'])):?>
                        <div class="shopInfo">
                            <?php foreach($list['content']['shop_info'] as $shop_info):?>
                                <p class="list">
                                    <span class="label"><?= $shop_info['label'];?></span>
                                    <span class="txt"><?= $shop_info['txt'];?></span>
                                </p>
                            <?php endforeach;?>
                        </div>
                    <?php endif;?>
                </div>
            </div>
            <hr>
        <?php endforeach;?>
    <?php endif;?>
</div>