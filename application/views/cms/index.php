<?php $this->breadcrumb_lib->set_title('<i class="fa fas fa-list"></i>  News List  ');?>
<?php $this->breadcrumb_lib->set_bc(['text'=>'  News List ', 'url'=>$this->site_url.'home/clear']);?>
<?php $this->script_lib->set_script($this->assets.'js/demo/form-layout.js')?>
<?php $this->script_lib->set_script($this->assets.'plugins/bootstrap-select/bootstrap-select.min.js')?>
<?php $this->script_lib->set_script($this->assets.'plugins/chosen/chosen.jquery.min.js')?>
<?php $this->script_lib->set_css($this->assets.'plugins/bootstrap-select/bootstrap-select.min.css')?>
<?php $this->script_lib->set_css($this->assets.'plugins/chosen/chosen.min.css')?>
<?php $this->script_lib->set_ele_js('elements/admin/js/list_js.php');?>
<?php $this->script_lib->set_ele_js('elements/js/delete_js.php');?>
<div class="row" style="margin-top: 100px;">
    <div class="col-md-12">
        <!-- Basic Data Tables -->
        <!--===================================================-->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">
                    News List
                    <span class="pull-right">
                        <button 
                            class="btn btn-success btn-labeled fa fa-plus"
                            onclick="location.href='<?= base_url()?>cms/home/add'"
                        >
                            News add
                        </button>
                    </span>
                </h3>
            </div>
            <div class="panel-body">
                <?php require(APPPATH."views/elements/flash_msg.php");?>
                <div class="table-responsive">
                    <table id="list" class="table table-striped table-bordered" width="100%">
                        <thead>
                            <tr>
                                <th width="10%"></th>
                                <th width="10%">ID</th>
                                <th width="20%" class="min-desktop">FROM</th>
                                <th width="20%" class="min-desktop">TO</th>
                                <th width="30%" class="min-desktop">NEWS</th>
                                <th width="10%" class="min-desktop"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($news)):?>
                                <?php foreach($news as $new): ?>
                                    <tr>
                                        <td class="text-center">
                                            <?= getStatusText($new)?>
                                        </td>
                                        <td><?= h($new['id'])?></td>
                                        <td><?= h($new['publish_from'])?></td>
                                        <td><?= h($new['publish_to'])?></td>
                                        <td><?= nl2br(h($new['content']))?></td>
                                        <td>
                                            <button 
                                                type="button" 
                                                class="btn btn-danger btn-icon icon-md fa fa-trash btn-delete"
                                                data-url="<?= $this->site_url;?>home/delete?id=<?= h($new['id'])?>"
                                                data-msg="NEWS ID:<?= h($new['id'])?>"
                                                data-toggle="tooltips" 
                                                data-placement="top" 
                                                title="delete"
                                            ></button>
                                            <a href="<?= $this->site_url;?>home/edit?id=<?= h($new['id'])?>">
                                                <button 
                                                    class="btn btn-info btn-icon icon-md fa fa-edit btn-edit"
                                                    data-toggle="tooltips" 
                                                    data-placement="top" 
                                                    title="edit"
                                                ></button>
                                            </a>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                            <?php endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
