<?php $this->breadcrumb_lib->set_title('<i class="fa fas fa-bullhorn"></i> CMS News Add');?>
<?php $this->breadcrumb_lib->set_bc(['text'=>'News List', 'url'=>$this->site_url.'home']);?>
<?php $this->breadcrumb_lib->set_bc(['text'=>'CMS News Add', 'url'=>$this->site_url.'home/add']);?>
<?php $this->script_lib->set_script($this->assets.'js/demo/form-layout.js')?>
<?php $this->script_lib->set_script($this->assets.'plugins/bootstrap-select/bootstrap-select.min.js')?>
<?php $this->script_lib->set_script($this->assets.'plugins/chosen/chosen.jquery.min.js')?>
<?php $this->script_lib->set_css($this->assets.'plugins/bootstrap-select/bootstrap-select.min.css')?>
<?php $this->script_lib->set_css($this->assets.'plugins/chosen/chosen.min.css')?>
<?php $this->script_lib->set_ele_js('elements/js/datetimepicker_custom_js.php');?>
<?php $this->script_lib->set_ele_js('elements/admin/js/cms_js.php');?>
<div class="row">
    <div class="bounce-in-left">
        <div class="col-md-2 eq-box-md grid"></div>
        <div class="col-md-8 eq-box-md grid">
            <form role="form" action="#" method="post" class="form-horizontal" id="form">
                <?php require(APPPATH."views/elements/csrf_input.php");?>
                <div class="panel">
                    <div class="panel-heading">
                        <div class="panel-control">
                        </div>
                        <h3 class="panel-title">CMS News Add</h3>
                    </div>
                    <div class="panel-body">
                        <?php require(APPPATH."views/elements/flash_msg.php");?>
                        <?php require(APPPATH."views/elements/admin/news_entry_form.php");?>
                    </div>
                    <div class="panel-footer text-center">
                        <button 
                            type="button"
                            class="btn btn-warning btn-labeled fa fa-arrow-circle-left pull-left"
                            onclick="location.href='<?= $this->site_url;?>'"
                        >
                        News List
                        </button>
                        <button 
                            class="btn btn-info btn-labeled fa fa-save form-submit" 
                            type="button"
                            data-url="<?= $this->site_url;?>home/add"
                        >
                            ADD
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-2 eq-box-md grid"></div>
    </div>
</div>