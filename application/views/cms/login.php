<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $this->title;?></title>
    <link rel="shortcut icon" href="img/favicon.ico">
     <!--Bootstrap Stylesheet [ REQUIRED ]-->
     <link href="<?= $this->assets_url?>admin/css/bootstrap.min.css" rel="stylesheet">
    <!--Jasmine Stylesheet [ REQUIRED ]-->
    <link href="<?= $this->assets_url?>admin/css/style.css?v=<?= time();?>" rel="stylesheet">
    <link href="<?= $this->assets_url?>admin/css/main.css?v=<?= time();?>" rel="stylesheet">
    <!--Font Awesome [ OPTIONAL ]-->
    <link href="<?= $this->assets_url?>admin/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?= $this->assets_url?>admin/plugins/bootstrap-validator/bootstrapValidator.min.css" rel="stylesheet">
    <!-- <script src="https://kit.fontawesome.com/90905d5180.js" crossorigin="anonymous"></script> -->
    <link href="<?= $this->assets_url?>admin/plugins/animsition/css/animsition.min.css" rel="stylesheet">
    <!--SCRIPT-->
</head>
<!--TIPS-->
<!--You may remove all ID or Class names which contain "demo-", they are only used for demonstration. -->

<body style="background-color:#e9e9e9;">
    <div 
        id="container" 
        class="animsition-overlay"
        data-animsition-in-class="fade-in"
        data-animsition-in-duration="1000"
        data-animsition-out-class="fade-out"
        data-animsition-out-duration="800"
    >
        <!-- LOGIN FORM -->
        <!--===================================================-->
        <div class="lock-wrapper">
            <div class="row">
                <div class="col-xs-12">
                    <div class="lock-box">
                        <div class="main">
                            <h3><?= $this->title;?>(ログイン)</h3>
                            <div class="login-or">
                                <hr class="hr-or">
                            </div>
                            <form role="form" action="<?= $this->site_url;?>login/auth" method="post" id="login_form">
                                <?php require(APPPATH."views/elements/csrf_input.php");?>
                                <div class="form-group">
                                    <p class="text-danger text-center"><?= !empty($error_msg) ? $error_msg:'';?></p>
                                </div>
                                <div class="form-group">
                                    <label for="login_id" class="control-label">ログインID</label>
                                    <input 
                                        type="text" 
                                        class="form-control" 
                                        id="login_id"
                                        name="login_id"
                                        value="<?= !empty($login_id) ? $login_id:''?>"
                                    >
                                </div>
                                <div class="form-group">
                                    <!-- <a class="pull-right text-primary" href="<?= $this->site_url;?>forgetPassword">パスワード忘れ</a> -->
                                    <label for="inputPassword" class="control-label">パスワード</label>
                                    <input 
                                        type="password" 
                                        class="form-control" 
                                        id="inputPassword"
                                        name="password"
                                        autocomplete="new-password" 
                                    >
                                    <span 
                                        toggle="#password-field" 
                                        class="fa fa-fw fa-eye field_icon toggle-password" 
                                    >
                                    </span>
                                </div>
                                <button type="submit" class="btn btn-block btn-primary">
                                    <i class="fas fa-sign-in-alt"></i>　ログイン
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->
    <!--JAVASCRIPT-->
    <!--=================================================-->
    <!--jQuery [ REQUIRED ]-->
    <script src="<?= $this->assets_url?>admin/js/jquery-2.1.1.min.js"></script>
    <!--BootstrapJS [ RECOMMENDED ]-->
    <script src="<?= $this->assets_url?>admin/js/bootstrap.min.js"></script>
    <!--Fast Click [ OPTIONAL ]-->
    <script src="<?= $this->assets_url?>admin/plugins/fast-click/fastclick.min.js"></script>
    <!--Bootstrap Validator [ OPTIONAL ]-->
    <script src="<?= $this->assets_url?>admin/plugins/bootstrap-validator/bootstrapValidator.min.js"></script>
    <script src="<?= $this->assets_url?>admin/plugins/animsition/js/animsition.min.js"></script>
    <script src="<?= $this->assets_url?>admin/js/form_validation.js"></script>
    <?php require(APPPATH."views/elements/admin/js/animsition_custom_js.php");?>
    <script>
     localStorage.setItem('sub_menu', 'false');
    $(document).on('click', '.toggle-password', function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $("#inputPassword");
        input.attr('type') === 'password' ? input.attr('type','text') : input.attr('type','password')
    });
</script>
</body>

</html>
