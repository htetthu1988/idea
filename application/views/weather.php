<div class="page-titles title-grey pt30 pb20">
    <div class="container">
        <div class="row">
            <div class=" col-md-6">
                <h4><span>Weather</span></h4>
            </div>
        </div>
    </div>
</div>
<div class="container mb70 pt90 layout-container">
    <?php if(!empty($weathers['daily'])):?>
    <div class="weather-container">
        <?php foreach($weathers['daily'] as $daily):?>
            <div class="row">
                <div class="col-2">
                    <img src="http://openweathermap.org/img/wn/<?= $daily['weather'][0]['icon']?>.png" />
                </div>
                <div class="col-3 weather-text"><?= date('m月d日', $daily['dt'])?></div>
                <div class="col-2 weather-text">東京</div>
                <div class="col-3 weather-text"><?= $daily['weather'][0]['description']?></div>
                <div class="col-2 weather-text"><?=round($daily['temp']['day'])?>°C</div>
            </div>
        <?php endforeach;?>
    </div>
    <?php endif;?>
</div>
