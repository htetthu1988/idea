<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

class Admin_model extends CI_Model {
    private $table;
    public function __construct() {
        parent::__construct ();
        $this->load->database ();
        $this->load->library('encryption');
        $this->table = 'm_admin';
    }
    public function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
    
    public function update($data) {
        $this->db->where('id', $data['id']);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }
    public function getAdmin($data){
        $this->db->select('*');
        $this->db->from($this->table);
        foreach($data as $key=>$val){
            $this->db->where($key, $val);
        }
        $this->db->where('deleted is null');
        $query = $this->db->get();
        $getAdmin = $query->row_array();
        return $getAdmin;
    }
    public function getAdmins($data){
        $this->db->select('*');
        $this->db->from($this->table);
        foreach($data as $key=>$val){
            $this->db->where($key, $val);
        }
        $this->db->where('deleted is null');
        $query = $this->db->get();
        $getAdmins = $query->result_array();
        return $getAdmins;
    }
    public function getFields(){
        return $this->db->list_fields($this->table);
    }

    public function loginAdmin($data)
    {
        $admin = $this->getAdmin(['login_id'=>$data['login_id']]);
        $result=[];
        if(!empty($admin))
        {
            $decrypt_pass= $this->encryption->decrypt($admin['password']);
            if($data['password'] == $decrypt_pass){
                $result=$admin;
            }
        }
        return $result;
    }
}
