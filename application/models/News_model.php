<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

class News_model extends CI_Model {
    private $table;
    public function __construct() {
        parent::__construct ();
        $this->load->database ();
        $this->table = 't_news';
    }
    public function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($data, $condition=[]) {
        if(!empty($condition)){
            foreach ($condition as $key=>$val) {
                $this->db->where($key, $val);
            }
        }
        $this->db->update($this->table,$data);
        return $this->db->affected_rows();
    }

    public function getNew($data=[]){
        $this->db->select('*');
        $this->db->from($this->table);
        if(!empty($data)){
            foreach($data as $key=>$val){
                $this->db->where($key, $val);
            }
        }
        $this->db->where('deleted is null');
        $query = $this->db->get();
        return $query->row_array();
    }
    public function getNews($data=[]){
        $this->db->select('*');
        $this->db->from($this->table);
        if(!empty($data)){
            foreach($data as $key=>$val){
                $this->db->where($key, $val);
            }
        }
        
        $this->db->where('deleted is null');
        $this->db->order_by('id desc');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getFields(){
        return $this->db->list_fields($this->table);
    }

    public function getNewsLists()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('publish_from <=', date('Y-m-d H:i:s'));
        $this->db->where('publish_to >=', date('Y-m-d H:i:s'));
        $this->db->where('status ', 1);
        $this->db->where('deleted is null');
        $this->db->order_by('id desc');
        $this->db->limit(10,0);
        $query = $this->db->get();
        return $query->result_array();
    }
}
