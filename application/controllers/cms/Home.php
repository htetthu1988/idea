<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('news_model');
        $this->load->library('validate_lib');
    }
    /**
     *
     * 
     */
    public function index()
    {
        $news = $this->news_model->getNews();
        $this->layout->load('cms/index', compact('news'));
    }

    public function add()
    {
        $upd_flg=0;
        $new = getEntities($this->news_model->getFields());
        if(!empty($this->input->post('new'))){
            $new = $this->input->post('new');
            $this->validate_lib->news();
            if ($this->form_validation->run() == true) {
                $upd_flg=$this->news_model->insert($new);
            }
            if($upd_flg>0){
                $this->session->set_tempdata('success_flash', 'CMS NEWS Add Successful.', 1);
                redirect(base_url('cms/home'));
            }else{
                $this->session->set_tempdata('error_flash', 'CMS NEWS Add Fail.', 1);
            }
        }
        $this->layout->load('cms/add', compact('new'));
    }

    public function delete()
    {
        $flg=0;
        $id = $this->input->get('id');
        if(!empty($id)){
            $condi['id']=$id;
            $news=[
                'deleted'=>date('Y-m-d h:s:i'),
            ];
            $flg=$this->news_model->update($news, $condi);
            if($flg){
                $this->session->set_tempdata('success_flash', 'CMS NEWS Delete Successful.', 1);
            }else{
                $this->session->set_tempdata('error_flash', 'CMS NEWS Delete Fail.', 1);
            }
        }
        redirect($this->site_url);
    }


    public function edit()
    {
        $upd_flg=0;
        $id = $this->input->get('id');
        $new = $this->input->post('new');
        if(!empty($new['id'])){
            $id=$new['id'];
        }
        if(!empty($id)){
            $new = $this->news_model->getNew(['id'=>$id]);
            if(!empty($this->input->post('new'))){
                $new = $this->input->post('new');
                $this->validate_lib->news();
                if ($this->form_validation->run() == true) {
                    $upd_flg=$this->news_model->update($new, ['id'=>$new['id']]);
                }
                if($upd_flg>0){
                    $this->session->set_tempdata('success_flash', 'CMS NEWS Edit Successful.', 1);
                    redirect(base_url('cms/home'));
                }else{
                    $this->session->set_tempdata('error_flash', 'CMS NEWS Edit Fail.', 1);
                }
            }
            $this->layout->load('cms/edit', compact('new'));
        }else{
            redirect($this->site_url);
        }
    }
}
