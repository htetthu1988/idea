<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper( 'url' );
        $this->load->helper( 'common_helper' );
        $this->load->library ( 'session' );
        $this->load->model('admin_model');
        $this->assets_url = $this->config->item('assets');
        $this->load->library('encryption');
        $this->site_url = base_url().'cms/';
        $this->title = '管理者';
    }

    /**
     * 
     * 
     */
    public function index()
    {
        $this->load->view('cms/login');
    }

    public function auth()
    {
        $login_id = $this->input->post('login_id');
        $password = $this->input->post('password');
        if(!empty($login_id) && !empty($password)){
            $request['login_id'] = $login_id;
            $request['password'] = $password;
            $result = $this->admin_model->loginAdmin($request);
            if(!empty($result)){
                $this->session->set_userdata('admin_id', $result['id']);
                redirect(base_url('cms/home/clear'));
            }else{
                $data['login_id'] = $login_id;
                $data['error_msg'] = '※ログインIDまたはパスワードに誤りがあります。';
                $this->load->view('cms/login', $data);
            }
        }else{
            redirect(base_url('cms/login'));
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('admin_id');
        redirect(base_url('cms/login'));
    }
    public function encryptPass(){
		echo 'admin';
		echo '<br>';
        echo $this->encryption->encrypt('admin');
    }

    public function decryptPass()
    {
		echo 'A100003';
		echo '<br>';
        echo $this->encryption->decrypt('60c6dc4cd53aab2f0a2d0f61ddd00b59b6e3449eb9c30a6cf5cb63128012bb5e9553ab5bb874ce7dbed9e1696314edc39cf506cf4eb2fabf0159cd13282c3147CRymQYR7JNwMjP9hRZhn8odLSHFsvwmmnIIzC0KfHNc=');
    }
}
