<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Map extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
    }
    /**
     * scraping process
     * 
     */
    public function index()
    {
        $this->layout->load('map');
    }
}
