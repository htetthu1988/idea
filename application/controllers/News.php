<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('news_model');
    }
    /**
     * scraping process
     * 
     */
    public function index()
    {
        $news = $this->news_model->getNewsLists();
        $this->layout->load('news', compact('news'));
    }
}
