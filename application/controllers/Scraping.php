<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Scraping extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('scraper');
        $this->load->helper('scraping_helper');
        $this->link = 'http://www.lumine.ne.jp/';
    }


    /**
     * 
     * 
     */
    public function index()
    {
        $lists = [];
        try {
            $xml_url = "http://lumine.ne.jp/xml/cms_shop.xml";

            $curl = curl_init();
            curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1 );
            curl_setopt( $curl, CURLOPT_URL, $xml_url );

            $xml = curl_exec( $curl );
            curl_close( $curl );

            $xml = iconv('UTF-8', 'UTF-8//IGNORE', $xml);

            $document = new DOMDocument;
            $document->loadXML( $xml );

            $shopinfo = $document->getElementsByTagName("shopinfo");
            foreach( $shopinfo as $s_info ){
                $yakata_id = $this->getXmlValue($s_info->getElementsByTagName( "yakata_id" ));
                if($yakata_id == 3){
                    $mansion_name = '';
                    $list['link']= $this->link.'ikebukuro/news/news_details.php?id='. $this->getXmlValue($s_info->getElementsByTagName( "id" ));
                    $list['print_date']= $this->getXmlValue($s_info->getElementsByTagName( "print_date" ));
                    $title= $this->getXmlValue($s_info->getElementsByTagName( "title" ));

                    $shop_name= $this->getXmlValue($s_info->getElementsByTagName( "shop_name" ));
                    $shop_mansion_id= $this->getXmlValue($s_info->getElementsByTagName( "shop_mansion_id" ));
                    $shop_floor= $this->getXmlValue($s_info->getElementsByTagName( "shop_floor" ));
                    $new_icon= $this->getXmlValue($s_info->getElementsByTagName( "new_icon" ));
                    
                    if($shop_mansion_id == '01' || $shop_mansion_id == '04' || $shop_mansion_id == '17'){
                        $mansion_name = "ルミネ1";
                    }elseif($shop_mansion_id == '02' || $shop_mansion_id == '05' || $shop_mansion_id == '18'){
                        $mansion_name = "ルミネ2";
                    }
                    if($shop_name != ''){
                        $list['title'] = '【'.$shop_name.'】'.$mansion_name.$shop_floor.' '.$title;
                    }else{
                        $list['title'] = $title;
                    }
                    $list['new_icon'] = $new_icon==1? $this->link.'common/img/new_icon.jpg':'';
                    $image =  $this->getXmlValue($s_info->getElementsByTagName( "shop_info" )->item(0)->getElementsByTagName('image'));
                    if(!empty($image)){
                        $list['image'] = $this->link.'pict/'.$image;
                    }else{
                        $list['image'] = $this->link.'common/img/noimage.gif';
                    }
                    $list['content']=$this->scraping_anchor( $list['link'] );
                    $lists[] = $list;
                }
            }
        } catch (Exception $e) {

        } finally {
            $this->layout->load('scraping', compact('lists'));
        }
    }

    private function getXmlValue($xml)
    {
        return $xml->item(0)->nodeValue;
    }

    private function scraping_anchor($url)
    {
        $html = file_get_html($url);
        $result['news_txt'] = [];
        $result['shop_info'] = [];
        foreach($html->find('div.newsTxt') as $news_txt) {
            $result['news_txt'] = $news_txt->plaintext;
        }
        foreach($html->find('p.list') as $indx => $list) {
            foreach($list->find('span.label') as $label) {
                $result['shop_info'][$indx]['label'] = $label->plaintext;
            }
            foreach($list->find('span.txt') as $txt) {
                $result['shop_info'][$indx]['txt'] = $txt->plaintext;
            }
        }
        return $result;
    }
}
