<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Weather extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('curl_lib');
    }
    /**
     * weather process
     * 
     */
    public function index()
    {
        $weathers = $this->curl_lib->get_weather_request();
        $this->layout->load('weather', compact('weathers'));
    }
}
