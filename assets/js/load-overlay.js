
// 汎用loading
function loading() {
    $.LoadingOverlay("show");
    setTimeout(function(){
        $.LoadingOverlay("hide");
    }, 10000);
}

var myAjax = function(arg) {
    var opt = $.extend({}, $.ajaxSettings, arg);
    var jqXHR = $.ajax(opt);
    var defer = $.Deferred();

    $.LoadingOverlay("show");

    jqXHR.done(function(data, statusText, jqXHR) {
        $.LoadingOverlay("hide");
        defer.resolveWith(this, arguments);
    });

    jqXHR.fail(function(jqXHR, statusText, errorThrown) {
        $.LoadingOverlay("hide");
        if(jqXHR.status==403) {

        }

        defer.rejectWith(this, arguments);
    });
    jqXHR.always(function() {
    });

    return $.extend({}, jqXHR, defer.promise());
};