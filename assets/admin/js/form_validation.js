// Form-Validation.js
// ====================================================================
// This file should not be included in your project.
// This is just a sample how to initialize plugins or components.
//
// - Designbudy.com -


$(document).ready(function() {


    // FORM VALIDATION
    // =================================================================
    // Require Bootstrap Validator
    // http://bootstrapvalidator.com/
    // =================================================================


    // FORM VALIDATION FEEDBACK ICONS
    // =================================================================
    var faIcon = {
            valid: 'fa fa-check-circle fa-lg text-success',
            invalid: 'fa fa-times-circle fa-lg',
            validating: 'fa fa-refresh'
        }
        // FORM VALIDATION WITH POPOVER
        // =================================================================
        // Indicate where the error messages are shown with popover 
        // =================================================================

    $('#login_form').bootstrapValidator({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            login_id: {
                validators: {
                    notEmpty: {
                        message: '※ログインIDを入力してください。'
                    },
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: '※パスワードを入力してください。'
                    },
                }
            },
        }
    })
    $('#change_password_form').bootstrapValidator({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            login_id: {
                validators: {
                    notEmpty: {
                        message: '※ログインIDを入力してください。'
                    },
                }
            },
            old_password: {
                validators: {
                    notEmpty: {
                        message: '※現在のパスワードを入力してください。'
                    },
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: '※新しいパスワードを入力してください。'
                    },
                    identical: {
                        field: 'password_confirm',
                        message: '※確認パスワードと一致しません。正しく入力してください。'
                    }
                }
            },
            password_confirm: {
                validators: {
                    notEmpty: {
                        message: '※新しいパスワード(確認)を入力してください。'
                    },
                    identical: {
                        field: 'password',
                        message: '※新しいパスワードと一致しません。正しく入力してください。'
                    }
                }
            }
        }
    })

    $('#forget_password_form').bootstrapValidator({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            login_id: {
                validators: {
                    notEmpty: {
                        message: '※ログインIDを入力してください。'
                    },
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: '※メールアドレスを入力してください。'
                    },
                    emailAddress: {
                        message: '※メールアドレスを入力してください。'
                    }
                }
            }
        }
    })

    $('#self_change_password_form').bootstrapValidator({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            old_password: {
                validators: {
                    notEmpty: {
                        message: '※現在のパスワードを入力してください。'
                    },
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: '※新しいパスワードを入力してください。'
                    },
                    identical: {
                        field: 'password_confirm',
                        message: '※確認パスワードと一致しません。正しく入力してください。'
                    }
                }
            },
            password_confirm: {
                validators: {
                    notEmpty: {
                        message: '※新しいパスワード(確認)を入力してください。'
                    },
                    identical: {
                        field: 'password',
                        message: '※新しいパスワードと一致しません。正しく入力してください。'
                    }
                }
            }
        }
    })

});