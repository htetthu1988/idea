function modalErrorDisplay(title, message) {
    bootbox.alert({
        title: '<span class="text-danger">' + title + '</span>',
        buttons: {
            ok: {
                label: '<i class="fa fa-check"></i> OK',
                className: 'btn-primary bootbox-confirm-btn',
            }
        },
        message: '<span class="text-danger">' + message + '</span>'
    })
}

function modalDisplay(title, message) {
    bootbox.alert({
        title: title,
        buttons: {
            ok: {
                label: '<i class="fa fa-check"></i> OK',
                className: 'btn-primary bootbox-confirm-btn',
            }
        },
        message: '<span>' + message + '</span>'
    })
}