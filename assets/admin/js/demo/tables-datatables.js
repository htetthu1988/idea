// Tables-DataTables.js
// ====================================================================
// This file should not be included in your project.
// This is just a sample how to initialize plugins or components.
//
// - Designbudy.com -



$(document).ready(function() {


    // DATA TABLES
    // =================================================================
    // Require Data Tables
    // -----------------------------------------------------------------
    // http://www.datatables.net/
    // =================================================================

    $.fn.DataTable.ext.pager.numbers_length = 5;


    // Basic Data Tables with responsive plugin
    // -----------------------------------------------------------------
    $('#demo-dt-basic').dataTable({
        "responsive": true,
        "pagingType": "full_numbers",
        "language": getLanguageJson(),
        "columnDefs": [{
            "targets": [9],
            "orderable": false
        }]
    });

    // Row selection (single row)
    // -----------------------------------------------------------------
    var rowSelection = $('#demo-dt-selection').DataTable({
        "responsive": true,
        "language": {
            "paginate": {
                "previous": '<i class="fa fa-angle-left"></i>',
                "next": '<i class="fa fa-angle-right"></i>'
            }
        }
    });

    $('#demo-dt-selection').on('click', 'tr', function() {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        } else {
            rowSelection.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });






    // Row selection and deletion (multiple rows)
    // -----------------------------------------------------------------
    var rowDeletion = $('#demo-dt-delete').DataTable({
        "responsive": true,
        "language": {
            "paginate": {
                "previous": '<i class="fa fa-angle-left"></i>',
                "next": '<i class="fa fa-angle-right"></i>'
            }
        },
        "dom": '<"toolbar">frtip'
    });
    $('#demo-custom-toolbar').appendTo($("div.toolbar"));

    $('#demo-dt-delete tbody').on('click', 'tr', function() {
        $(this).toggleClass('selected');
    });

    $('#demo-dt-delete-btn').click(function() {
        rowDeletion.row('.selected').remove().draw(false);
    });






    // Add Row
    // -----------------------------------------------------------------
    var t = $('#demo-dt-addrow').DataTable({
        "responsive": true,
        "language": {
            "paginate": {
                "previous": '<i class="fa fa-angle-left"></i>',
                "next": '<i class="fa fa-angle-right"></i>'
            }
        },
        "dom": '<"newtoolbar">frtip'
    });
    $('#demo-custom-toolbar2').appendTo($("div.newtoolbar"));

    $('#demo-dt-addrow-btn').on('click', function() {
        t.row.add([
            'Albert Johnson',
            'New Row',
            'New Row',
            jasmine.randomInt(1, 100),
            '2015/10/15',
            '$' + jasmine.randomInt(1, 100) + ',000'
        ]).draw();
    });


});

function getLanguageJson() {
    return {
        "sEmptyTable": "一致するレコードがありません",
        "sInfo": " _TOTAL_ 件中 _START_ から _END_ まで表示",
        "sInfoEmpty": " 0 件中 0 から 0 まで表示",
        "sInfoFiltered": "（全 _MAX_ 件より抽出）",
        "sInfoPostFix": "",
        "sInfoThousands": ",",
        "sLengthMenu": "_MENU_ 件表示",
        "sLoadingRecords": "読み込み中...",
        "sProcessing": "処理中...",
        "sSearch": "検索:",
        "sZeroRecords": "一致するレコードがありません",
        "oPaginate": {
            "sFirst": "先頭",
            "sLast": "最終",
            "sNext": "次",
            "sPrevious": "前"
        },
        "oAria": {
            "sSortAscending": ": 列を昇順に並べ替えるにはアクティブにする",
            "sSortDescending": ": 列を降順に並べ替えるにはアクティブにする"
        },
        "paginate": {
            // "previous": '<i class="fa fa-angle-left"></i>',
            // "next": '<i class="fa fa-angle-right"></i>',
            // "first": '<i class="fa fa-angle-left"></i>',
            // "last": '<i class="fa fa-angle-right"></i>',
        }
    }
}